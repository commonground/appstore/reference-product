#!/bin/sh

# check if docker is running
docker ps

if [[ $? -ne 0 ]] ; then
    echo "Docker deamon seems to not be running or giving bad response."
    exit 1
fi

echo "Seems like Docker is running."

# check if psql is running
psql -l

if [[ $? -ne 0 ]] ; then
    echo "The check if PostgreSQL is running failed. Not going to continue running the other parts of the reference product."
    exit 1
fi

echo "Seems like Postgres is running, now let's spin up all check-in apps!"

# start services
ttab 'cd check-in-api;clear;pwd;modd;'
ttab 'cd check-in-outway;clear;pwd;docker run --rm -it -p 3003:3003 -e LISTEN_ADDRESS="0.0.0.0:3003" $(docker build -q .);'
ttab 'cd check-in-kiosk-processcomponent;clear;pwd;node index.js'
ttab 'cd check-in-dashboard-processcomponent;clear;pwd;node index.js'
ttab 'cd check-in-dashboard-ui;clear;pwd;http-server static -p 3001 --proxy http://localhost:3005;'
ttab 'cd check-in-kiosk-ui;clear;pwd;http-server static -p 3002 --proxy http://localhost:3004;'
