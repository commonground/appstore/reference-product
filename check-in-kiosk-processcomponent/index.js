var express = require('express')
var fetch = require('node-fetch')
var bodyParser = require('body-parser')
var app = express()

app.use(bodyParser.urlencoded({ extended: true }))

var port = process.env.PORT || 3004
var outwayAddress = process.env.OUTWAY_ADDRESS || 'http://localhost:3003'
var checkInAPIAddress = process.env.CHECK_IN_API_ADDRESS || 'http://localhost:3000'
var gemeente = process.env.GEMEENTE || 'Rotterdam'

app.get('/', function (req, res) {
    res.send('Check-in Kiosk Processcomponent: Hello, World!')
})

app.post('/api/check-ins', function (req, res) {
    var bsn = req.body.bsn

    getBRPInfoForBSN(bsn)
        .catch(err => {
            console.info('failed to get BRO info for BSN: ', err.message)
            return null
        })
        .then(person => person ? getWoonplaatsFromBRPResponse(person) : null)
        .then(woonplaats =>
            woonplaats ?
                registerCheckIn(woonplaats)
                    .then(() => woonplaats) :
                woonplaats
        )
        .then(woonplaats => {
            var toegewezenBalie = null

            if (woonplaats === gemeente) {
                toegewezenBalie = getBalieForAfspraakByBsn(bsn)
            } else {
                toegewezenBalie = 'balie-onbekend'
            }

            console.log(`referring visitor to ${toegewezenBalie}`)
            res.redirect(`${req.headers.referer}bedankt-${toegewezenBalie}.html`)
        })
})

var getBRPInfoForBSN = bsn =>
    fetch(`${outwayAddress}/brp/basisregistratie/natuurlijke_personen/bsn/${bsn}`)
        .then(res => {
            if (res.status === 404) {
                throw new Error(`Geen persoonsgegevens beschikbaar voor BSN ${bsn}`)
            }

            return res.json()
        })

var getWoonplaatsFromBRPResponse = brpResponse =>
    brpResponse.verblijfadres.woonplaats

var getBalieForAfspraakByBsn = bsn => {
    var afspraken = { '283074796': ['balie1'] }
    return afspraken[bsn] || 'balie-onbekend'
}

var registerCheckIn = municipality =>
    fetch(`${checkInAPIAddress}/api/check-ins`, {
        method: 'POST',
        body: JSON.stringify({ municipality })
    })
        .catch(err => {
            console.info('failed to register check-in: ', err.message)
        })

app.listen(port);
console.log(`Running on port ${port}`);
