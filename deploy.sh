#!/bin/sh

PREFIX=${1:?Namespace prefix}-
VERSION=${2:?Chart version}
TLD=${3:-minikube}
GEMEENTE=${4:-Rotterdam}

_helm () {
    name=$1
    short_name=${2}
    domain=${PREFIX}${1}.${TLD}
    helm install \
        --debug \
        ./helm/${name}-${VERSION}.tgz \
        --namespace ${PREFIX}${short_name} \
        --name ${PREFIX}${short_name} \
        --set domain=${domain} \
        --set dashboardProcesscomponentNamespace=${PREFIX}check-in-dp \
        --set kioskProcesscomponentNamespace=${PREFIX}check-in-kp \
        --set outwayNamespace=${PREFIX}check-in-outway \
        --set apiNamespace=${PREFIX}check-in-api \
        --set gemeente=${GEMEENTE}
}

_helm check-in-api check-in-api
_helm check-in-outway check-in-outway
_helm check-in-dashboard-processcomponent check-in-dp
_helm check-in-dashboard-ui check-in-du
_helm check-in-kiosk-processcomponent check-in-kp
_helm check-in-kiosk-ui check-in-ku
