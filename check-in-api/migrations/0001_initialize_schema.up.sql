--- Explanation 25 chars for the municipality name:
--- "Gasselterboerveenschemond (25 letters) wordt algemeen als de langste plaatsnaam van Nederland beschouwd"
--- via https://onzetaal.nl/nieuws-en-dossiers/dossiers/taalrecords/de-langste-plaatsnaam-van-nederland-en-belgi

CREATE TABLE "check-ins" (
    timestamp timestamp default now(),
    municipality varchar(25) not null
);
