package resources

import (
	"database/sql"
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/commonground/appstore/reference-product/check-in-api/services"
	"go.uber.org/zap"
	"net/http"
	"sort"
	"time"
)

// CheckInResource enables injecting functions to deal with the CheckIn resource handlers
type CheckInResource struct {
	Logger         *zap.Logger
	DB             *sql.DB
	CheckInService *services.CheckInService
}

// NewCheckInResource creates a new CheckInResource
func NewCheckInResource(logger *zap.Logger, db *sql.DB) *CheckInResource {
	i := &CheckInResource{
		Logger:         logger,
		DB:             db,
		CheckInService: services.NewCheckInService(logger, db),
	}

	return i
}

// Routes defines the routes for the API resource
func (rs CheckInResource) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", rs.Add)
	r.Get("/stats", rs.Stats)

	return r
}

// Add will register a new check-in
func (rs CheckInResource) Add(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	response_body := struct {
		Municipality string `json:"municipality"`
	}{}

	err := json.NewDecoder(r.Body).Decode(&response_body)

	if err != nil {
		rs.Logger.Error("could not parse json from the request body", zap.Error(err))
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	err = rs.CheckInService.Add(response_body.Municipality)

	if err != nil {
		rs.Logger.Error("could not add check-in", zap.Error(err))
		http.Error(w, "bad request", http.StatusInternalServerError)
		return
	}

	http.StatusText(http.StatusCreated)
}

type Dataset struct {
	Label string `json:"label"`
	Data  []int  `json:"data"`
}

type statsResponse struct {
	Labels   []string  `json:"labels"`
	Datasets []Dataset `json:"datasets"`
}

func createDateLabels(from_date time.Time, to_date time.Time) []string {
	amount_of_days := int(to_date.Sub(from_date).Hours() / 24)

	dates := []string{}

	for i := 1; i <= amount_of_days; i++ {
		date := from_date.Add(time.Hour * time.Duration(24*i))
		date_formatted := date.Format("2006-01-02")
		dates = append(dates, date_formatted)
	}

	return dates
}

func uniqueMunicipalitiesFromStats(all_stats []services.Stats) []string {
	result := []string{}
	municipalities_map := map[string]string{}

	for _, stats := range all_stats {
		municipalities_map[stats.Municipality] = stats.Municipality
	}

	for municipality := range municipalities_map {
		result = append(result, municipality)
	}

	sort.Strings(result)

	return result
}

func createStatsResponse(from_date time.Time, to_date time.Time, all_stats []services.Stats) statsResponse {
	result := statsResponse{
		Labels:   createDateLabels(from_date, to_date),
		Datasets: []Dataset{},
	}

	// create map from stats
	municipalities := uniqueMunicipalitiesFromStats(all_stats)
	stats_map := map[string]map[string]int{}

	for _, stats := range all_stats {
		stats_map_by_period, ok := stats_map[stats.Period]

		if !ok {
			stats_map_by_period = make(map[string]int)
			stats_map[stats.Period] = stats_map_by_period
		}

		stats_map[stats.Period][stats.Municipality] = stats.AmountOfCheckins
	}

	for _, municipality := range municipalities {
		data := []int{}

		for _, date := range result.Labels {
			_, data_for_date := stats_map[date]

			var result int

			if !data_for_date {
				result = 0
			}

			amountOfCheckins, data_for_municipality_for_date := stats_map[date][municipality]

			if !data_for_municipality_for_date {
				result = 0
			}

			result = amountOfCheckins
			data = append(data, result)
		}

		result.Datasets = append(result.Datasets, Dataset{
			Label: municipality,
			Data:  data,
		})
	}

	return result
}

// Stats will return statistics for the check-ins
func (rs CheckInResource) Stats(w http.ResponseWriter, r *http.Request) {
	to_date := time.Now()
	from_date := to_date.AddDate(0, -1, 0)

	stats := rs.CheckInService.GetStatsGroupedByDayAndMunicipality(from_date, to_date)
	newResponse := createStatsResponse(from_date, to_date, stats)

	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(newResponse)

	if err != nil {
		rs.Logger.Error("failed to get statistics for check-ins", zap.Error(err))
		http.Error(w, "server error", http.StatusInternalServerError)
		return
	}
}
