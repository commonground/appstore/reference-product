// Copyright © VNG Realisatie 2019
// Licensed under the EUPL

package main

import (
	"fmt"
	api "gitlab.com/commonground/appstore/reference-product/check-in-api"
	"go.nlx.io/nlx/common/logoptions"
	"log"
	"time"

	"database/sql"
	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"

	"github.com/cenkalti/backoff"
	_ "github.com/lib/pq"
)

var options struct {
	ListenAddressPlain string `long:"listen-address-plain" env:"LISTEN_ADDRESS_PLAIN" default:"0.0.0.0:3000" description:"Address for the API to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	PostgresDSN        string `long:"postgres-dsn" env:"POSTGRES_DSN" default:"postgres://postgres:postgres@localhost/check-ins?sslmode=disable" description:"Postgres Connection String. See https://godoc.org/github.com/lib/pq#hdr-Connection_String_Parameters."`
	MigrationsPath     string `long:"migrations-path" default:"./migrations" env:"MIGRATIONS_PATH" description:"Path to directory where the migration files are located."`

	logoptions.LogOptions
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	// Setup new zap logger
	config := options.LogOptions.ZapConfig()
	logger, err := config.Build()

	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	// Setup db connection
	db, err := sql.Open("postgres", options.PostgresDSN)

	if err != nil {
		log.Fatal(err)
	}

	pingDatabase := func() error {
		return db.Ping()
	}

	notifyIt := func(err error, duration time.Duration) {
		logger.Error(fmt.Sprintf("failed to connect to the database, retrying in %f seconds", duration.Seconds()), zap.Error(err))
	}

	err = backoff.RetryNotify(pingDatabase, backoff.NewExponentialBackOff(), notifyIt)

	if err != nil {
		return
	}

	api.PerformMigration(logger, fmt.Sprintf("file://%s", options.MigrationsPath), options.PostgresDSN)

	apiServer := api.NewServer(logger, db)
	logger.Info("API running on", zap.String("address", options.ListenAddressPlain))

	// Listen on the address provided in the options
	err = apiServer.ListenAndServe(options.ListenAddressPlain)
	if err != nil {
		logger.Fatal("failed to listen and serve", zap.Error(err))
	}
}
