// Copyright © VNG Realisatie 2019
// Licensed under the EUPL

package check_in_api

import (
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"go.uber.org/zap"
)

func PerformMigration(logger *zap.Logger, sourceURL string, databaseURL string) {
	logger.Info("migration starting")
	m, err := migrate.New(sourceURL, databaseURL)

	if err != nil {
		logger.Fatal("failed to start migrations", zap.Error(err))
	}

	err = m.Up()

	if err == migrate.ErrNoChange {
		logger.Info("no changes")
	} else if err != nil {
		logger.Fatal("failed to perform migrations", zap.Error(err))
	}

	logger.Info("migration completed")
}
