// Copyright © VNG Realisatie 2019
// Licensed under the EUPL

package check_in_api

import (
	"database/sql"
	"net/http"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// Server is the server itself
type Server struct {
	Logger *zap.Logger
	DB     *sql.DB
}

// NewServer creates a new Server and sets it up to handle requests.
func NewServer(l *zap.Logger, db *sql.DB) *Server {
	i := &Server{
		Logger: l,
		DB:     db,
	}
	return i
}

// ListenAndServe is a blocking function that listens to the provided TCP address to handle requests.
func (api *Server) ListenAndServe(address string) error {
	r := router(api.Logger, api.DB)

	err := http.ListenAndServe(address, r)
	if err != nil {
		return errors.Wrap(err, "failed to run http server")
	}
	return nil
}
