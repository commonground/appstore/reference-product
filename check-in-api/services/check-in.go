package services

import (
	"database/sql"
	"fmt"
	"go.uber.org/zap"
	"time"
)

// CheckInService
type CheckInService struct {
	Logger *zap.Logger
	DB     *sql.DB
}

// NewCheckInService creates a new CheckInService
func NewCheckInService(logger *zap.Logger, db *sql.DB) *CheckInService {
	i := &CheckInService{
		Logger: logger,
		DB:     db,
	}

	return i
}

type Stats struct {
	Period           string `json:"period"`
	Municipality     string `json:"municipality"`
	AmountOfCheckins int    `json:"amountOfCheckins"`
}

func (s CheckInService) GetStatsGroupedByDayAndMunicipality(from_date time.Time, to_date time.Time) []Stats {
	result := []Stats{}

	rows, err := s.DB.Query("SELECT municipality, DATE(timestamp) as period, COUNT(*) as amount_of_checkins FROM \"check-ins\" WHERE \"timestamp\" BETWEEN $1 AND $2 GROUP by period, municipality", from_date, to_date)

	if err != nil {
		s.Logger.Error("failed fetching Stats from DB", zap.Error(err))
	}

	for rows.Next() {
		theStats := Stats{}
		err = rows.Scan(&theStats.Municipality, &theStats.Period, &theStats.AmountOfCheckins)

		if err != nil {
			s.Logger.Error("failed reading row", zap.Error(err))
		}

		// workaround to remove timestamp from date, see https://github.com/lib/pq/issues/589
		theStats.Period = theStats.Period[0:10]

		result = append(result, theStats)
	}

	return result
}

func (s CheckInService) Add(municipality string) error {
	rows, err := s.DB.Query("INSERT INTO \"check-ins\" (municipality) VALUES ($1)", municipality)

	if err != nil {
		s.Logger.Error("failed inserting check-in into DB", zap.Error(err))
	}

	fmt.Print(rows)

	return err
}
