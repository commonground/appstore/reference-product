module gitlab.com/commonground/appstore/reference-product/check-in-api

go 1.12

require (
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang-migrate/migrate/v4 v4.5.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/lib/pq v1.2.0
	github.com/pkg/errors v0.8.1
	go.nlx.io/nlx v0.74.1
	go.uber.org/zap v1.10.0
)
