package models

// CheckIn model
type CheckIn struct {
	Timestamp    string `json:"timestamp,omitempty"`
	Municipality string `json:"municipality"`
}
