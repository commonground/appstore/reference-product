package models

// CheckInStats model
type CheckInStats struct {
	GroupedBy string
	Data      []CheckInStatsData
}

// CheckInStatsData model
type CheckInStatsData struct {
	Date  string
	Value int
}
