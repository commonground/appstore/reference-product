# Running locally without much tooling

## Prerequisites

* [Go](https://golang.org/doc/install)
* [Node.js](https://nodejs.org/) (with NPM)
* [Modd](https://github.com/cortesi/modd)
* [PostgreSQL](https://www.postgresql.org)
* [ttab](https://www.npmjs.com/package/ttab)
* [http-server](https://www.npmjs.com/package/http-server)

> Note: the `dev.sh` script will execute the commands below for you.
> Please make sure to install the dependencies first.

## Check-in data

Install PostgreSQL and add a database named `check-ins`.

## Check-in API

```shell script
cd check-in-api
go get ./...
modd
```

Will be available at [http://localhost:3000](http://localhost:3000)

## Dashboard UI

```shell script
cd check-in-dashboard-ui
http-server static -p 3001 --proxy http://localhost:3000
```

Will be available at [http://localhost:3001](http://localhost:3001)

## Kiosk UI

```shell script
cd check-in-kiosk-ui
http-server static -p 3002 --proxy http://localhost:3004
```

Will be available at [http://localhost:3002](http://localhost:3002)

# Outway

```shell script
cd check-in-outway
docker run --rm -it -p 3003:3003 -e LISTEN_ADDRESS='0.0.0.0:3003' $(docker build -q .)
```

Will be available at [http://localhost:3003](http://localhost:3003)

# Kiosk Processcomponent

```shell script
cd check-in-kiosk-processcomponent
npm install
node index.js
```

Will be available at [http://localhost:3004](http://localhost:3004)

# Dashboard Processcomponent

```shell script
cd check-in-dashboard-processcomponent
npm install
node index.js
```

Will be available at [http://localhost:3005](http://localhost:3005)
