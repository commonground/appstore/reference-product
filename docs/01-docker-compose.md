## Running locally using Docker Compose

```shell script
docker-compose up --build
```

| Application | URL  |
|-------------|------|
| Check-in API | [http://localhost:8000](http://localhost:8000) |
| Dashboard UI | [http://localhost:8001](http://localhost:8001) |
| Kiosk UI |  [http://localhost:8002](http://localhost:8002) |
| Outway |  [http://localhost:8003](http://localhost:8003) |
| Check-in Kiosk Processcomponent | [http://localhost:8004](http://localhost:8004) |
| Check-in Dashboard Processcomponent | [http://localhost:8005](http://localhost:8005) |
