## Running locally on a Kubernetes cluster

### Prerequisites

Make sure you have installed the following tools:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [helm](https://docs.helm.sh/using_helm/)

### Installation

Below are the installation steps to run the application on a local Kubernetes cluster using Helm.
Let's start by cloning this repository.

#### Preparation

```bash
git clone https://gitlab.com/commonground/appstore/reference-product
cd reference-product
```

Start minikube.

```bash
minikube start
```

Initialize Helm.

```bash
helm init
helm repo update
```

Next, install Traefik as ingress controller.

```bash
helm install stable/traefik --name traefik --namespace traefik --values helm/traefik-values-minikube.yaml
```

#### Install applications

```shell script
cd helm
helm install --namespace check-in-api --name check-in-api ./check-in-api
helm install --namespace check-in-du --name check-in-du ./check-in-dashboard-ui
helm install --namespace check-in-dp --name check-in-dp ./check-in-dashboard-processcomponent
helm install --namespace check-in-ku --name check-in-ku ./check-in-kiosk-ui
helm install --namespace check-in-kp --name check-in-kp ./check-in-kiosk-processcomponent
helm install --namespace check-in-outway --name check-in-outway ./check-in-outway
sh initialize-hostnames.sh
```

#### Testing

* [check-in-dashboard-ui.minikube](http://check-in-dashboard-ui.minikube/)
* [heck-in-kiosk-ui.minikube](http://check-in-kiosk-ui.minikube/)
