App Store - Reference Product
=============================

The different components which are part of the App Store reference product.

# Customer journey

![persoonlijke-richtingwijzing](./docs/customer-journey/checkin.png)
Dowload the draw.io source file: [download](./docs/customer-journey/checkin.drawio)

# BPMN process

![persoonlijke-richtingwijzing](./docs/bpmn/checkin.svg)
Download the bpmn.io source file [download](./docs/bpmn/checkin.bpmn)

## Please mind that:

* The application is made to prove the auto-deploy feature of the AppStore, it is not suitable to use in production
* We don't expose the check-in-data over NLX because the data is intended only for the municipality running this application
* The Processcomponents are very limited to make sure their purpose is made clear. They don't contain rate-limiting, authentication etc.

## Mock data

We use the [BRP mock service](https://gitlab.com/commonground/nlx-demo/tree/4f97cbdd/brp-mock-service) to relate a BSN number to a person's information.

## Prerequisites

All ways methods of running the app require their own tooling.
What you do need for all of them is at least:

* [Git](https://git-scm.com/)
* [Docker](https://www.docker.com/)

## Instructions on running the app

* [🙅‍️ Without tooling](./docs/00-raw.md), recommended for local development.
* [🐳 Using Docker Compose](./docs/01-docker-compose.md), recommended for demo's.
* [☸️ Using Kubernetes](./docs/02-kubernetes.md), recommended for live environments.
