var express = require('express')
var fetch = require('node-fetch')
var app = express()

var port = process.env.PORT || 3005
var checkInAPIAddress = process.env.CHECK_IN_API_ADDRESS || 'http://localhost:3000'

app.get('/', function (req, res) {
    res.send('Check-in Dashboard Processcomponent: Hello, World!')
})

app.get('/api/check-ins/stats', function (req, res) {
    getCheckInStats()
        .catch(err => {
            console.info(err.message)
            return null
        })
        .then(response => {
            res.json(response)
        })
})

var getCheckInStats = () =>
    fetch(`${checkInAPIAddress}/api/check-ins/stats`)
        .then(response => response.json())

app.listen(port);
console.log(`Running on port ${port}`);
