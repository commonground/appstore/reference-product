#!/bin/bash

PREFIX=${1}${1:+\-}
minikube_ip=$(minikube ip)
echo "${minikube_ip}                        ${PREFIX}check-in-api.minikube"              | sudo tee -a /etc/hosts
echo "${minikube_ip}                    ${PREFIX}check-in-dashboard-ui.minikube"         | sudo tee -a /etc/hosts
echo "${minikube_ip}                      ${PREFIX}check-in-kiosk-ui.minikube"           | sudo tee -a /etc/hosts
echo "${minikube_ip}                       ${PREFIX}check-in-outway.minikube"            | sudo tee -a /etc/hosts
echo "${minikube_ip}              ${PREFIX}check-in-kiosk-processcomponent.minikube"     | sudo tee -a /etc/hosts
echo "${minikube_ip}           ${PREFIX}check-in-dashboard-processcomponent.minikube"    | sudo tee -a /etc/hosts
